import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.cyan,
      ),
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Color _color;
  TextStyle _textStyle;
  @override
  void setState(fn) {
    // TODO: implement setState
    super.setState(fn);
  }
  void _change_color(){
    Random  rand = new Random();
    double   size  = rand.nextDouble();
    setState(() {
      _color = Color.fromARGB(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
      _textStyle = TextStyle(
        color: Color.fromARGB(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255)),
        fontSize: doubleInRange(rand, 20, 50)
      );
    });
  }
  double doubleInRange(Random source, int start, int end) =>
      source.nextDouble() * (end - start) + start;

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: _change_color,
      child: Scaffold(
//      backgroundColor: _color,
        body:GestureDetector(
          child: Container(
            color: _color,
            child: Center(
              child: Container(child: Text("Hey there",style: _textStyle,)),
            ),
          ),
        ),
      ),
    );
  }
}
